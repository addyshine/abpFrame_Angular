﻿using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace abpAngular.Web.Authentication.External
{
    public class OpenIdConnectAuthProviderApi : ExternalAuthProviderApiBase
    {
        public const string Name = "OpenIdConnect";

        public override async Task<ExternalAuthUserInfo> GetUserInfo(string token)
        {
            string text = base.ProviderInfo.AdditionalParams["Authority"];
            if (string.IsNullOrEmpty(text))
            {
                throw new ApplicationException("Authentication:OpenId:Issuer configuration is required.");
            }
            ConfigurationManager<OpenIdConnectConfiguration> configurationManager = new ConfigurationManager<OpenIdConnectConfiguration>(text + "/.well-known/openid-configuration", new OpenIdConnectConfigurationRetriever(), new HttpDocumentRetriever());
            JwtSecurityToken val = await ValidateToken(token, text, configurationManager);
            string value = val.Claims.First((Claim c) => c.Type == "name").Value;
            string value2 = val.Claims.First((Claim c) => c.Type == "unique_name").Value;
            string[] array = value.Split(' ');
            return new ExternalAuthUserInfo
            {
                Provider = "OpenIdConnect",
                ProviderKey = val.Subject,
                Name = array[0],
                Surname = array[1],
                EmailAddress = value2
            };
        }

        private async Task<JwtSecurityToken> ValidateToken(string token, string issuer, IConfigurationManager<OpenIdConnectConfiguration> configurationManager, CancellationToken ct = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new ArgumentNullException("token");
            }
            if (string.IsNullOrEmpty(issuer))
            {
                throw new ArgumentNullException("issuer");
            }
            ICollection<SecurityKey> signingKeys = (await configurationManager.GetConfigurationAsync(ct)).SigningKeys;
            TokenValidationParameters val = new TokenValidationParameters();
            val.ValidateIssuer = true;
            val.ValidIssuer = issuer;
            val.ValidateIssuerSigningKey = true;
            val.IssuerSigningKeys = (IEnumerable<SecurityKey>)signingKeys;
            val.ValidateLifetime = true;
            val.ClockSkew = TimeSpan.FromMinutes(5.0);
            val.ValidateAudience = false;
            TokenValidationParameters val2 = val;
            SecurityToken val3 = default(SecurityToken);
            ClaimsPrincipal claimsPrincipal = new JwtSecurityTokenHandler().ValidateToken(token, val2, out val3);
            if (base.ProviderInfo.ClientId != claimsPrincipal.Claims.First((Claim c) => c.Type == "aud").Value)
            {
                throw new ApplicationException("ClientId couldn't verified.");
            }
            return (JwtSecurityToken)val3;
        }

    }

}
