﻿using Abp.Notifications;
using abpAngular.Dto;

namespace abpAngular.Notifications.Dto
{
    public class GetUserNotificationsInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }
    }
}