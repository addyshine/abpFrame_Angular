﻿using System.Collections.Generic;
using abpAngular.Chat.Dto;
using abpAngular.Dto;

namespace abpAngular.Chat.Exporting
{
    public interface IChatMessageListExcelExporter
    {
        FileDto ExportToFile(List<ChatMessageExportDto> messages);
    }
}
