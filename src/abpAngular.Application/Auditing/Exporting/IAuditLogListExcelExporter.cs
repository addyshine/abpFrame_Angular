﻿using System.Collections.Generic;
using abpAngular.Auditing.Dto;
using abpAngular.Dto;

namespace abpAngular.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);

        FileDto ExportToFile(List<EntityChangeListDto> entityChangeListDtos);
    }
}
