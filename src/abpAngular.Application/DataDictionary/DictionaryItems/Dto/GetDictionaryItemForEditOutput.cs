﻿using abpAngular.DataDictionary.DictionaryValues.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace abpAngular.DataDictionary.DictionaryItems.Dto
{
    public class GetDictionaryItemForEditOutput
    {
        public DictionaryItemEditDto DictionaryItem { get; set; }

        public List<DictionaryValueListDto> DictionaryValues { get; set; }
    }
}
