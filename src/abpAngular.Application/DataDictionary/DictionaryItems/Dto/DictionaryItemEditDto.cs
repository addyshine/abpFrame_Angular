﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace abpAngular.DataDictionary.DictionaryItems.Dto
{
    public class DictionaryItemEditDto
    {
        public int? Id { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "字典名称超出最大长度")]
        public string Name { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "字典标识超出最大长度")]
        public string Code { get; set; }

        public int? ParentId { get; set; }

        public int SortCode { get; set; }
    }
}
