﻿using System.ComponentModel.DataAnnotations;

namespace abpAngular.DataDictionary.DictionaryItems.Dto
{
    public class CreateOrUpdateDictionaryItemInput
    {
        [Required]
        public DictionaryItemEditDto DictionaryItem { get; set; }
    }
}
