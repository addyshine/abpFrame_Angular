﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace abpAngular.DataDictionary.DictionaryItems.Dto
{
    public class DictionaryItemListDto: EntityDto
    {
        public string Name { get; set; }
        public string Code { get; set; }

        public int ParentId { get; set; }

        public int SortCode { get; set; }
    }
}
