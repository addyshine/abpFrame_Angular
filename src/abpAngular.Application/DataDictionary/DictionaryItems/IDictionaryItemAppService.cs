﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using abpAngular.DataDictionary.DictionaryItems.Dto;

namespace abpAngular.DataDictionary.DictionaryItems
{
    /// <summary>
    /// 数据字典服务接口
    /// </summary>
    public interface IDictionaryItemAppService: IApplicationService
    {
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns></returns>
        Task<ListResultDto<DictionaryItemListDto>> GetAll();

        /// <summary>
        /// 根据id获取单条数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<DictionaryItemListDto> GetById(EntityDto input);

        /// <summary>
        /// 获取用于更新的View数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<GetDictionaryItemForEditOutput> GetForEdit(NullableIdDto input);

        /// <summary>
        /// 新建或编辑
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<DictionaryItemListDto> CreateOrUpdate(CreateOrUpdateDictionaryItemInput input);

        /// <summary>
        /// 新建
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<DictionaryItemListDto> Create(CreateOrUpdateDictionaryItemInput input);

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<DictionaryItemListDto> Update(CreateOrUpdateDictionaryItemInput input);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task Delete(EntityDto input);

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task BatchDelete(List<long> input);
    }
}
