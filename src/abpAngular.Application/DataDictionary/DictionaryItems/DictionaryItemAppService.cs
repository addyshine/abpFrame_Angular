﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using abpAngular.DataDictionary.DictionaryItems.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Microsoft.EntityFrameworkCore;
using abpAngular.DataDictionary.DictionaryValues.Dto;
using Abp.Authorization;
using abpAngular.Authorization;

namespace abpAngular.DataDictionary.DictionaryItems
{
    /// <summary>
    /// 数据字典服务
    /// </summary>
    public class DictionaryItemAppService : AbpFrameAppServiceBase, IDictionaryItemAppService
    {
        private readonly IRepository<DictionaryItem> _repository;
        private readonly IRepository<DictionaryValue> _dictionaryValueRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="dictionaryValueRepository"></param>
        public DictionaryItemAppService(
            IRepository<DictionaryItem> repository,
            IRepository<DictionaryValue> dictionaryValueRepository)
        {
            _repository = repository;
            _dictionaryValueRepository = dictionaryValueRepository;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns></returns>
        [AbpAuthorize(DictionaryItemPermissions.Node)]
        public async Task<ListResultDto<DictionaryItemListDto>> GetAll()
        {
            var query = _repository.GetAll();
            var count = await query.CountAsync();

            var entityList = await query.OrderBy(t=>t.SortCode).ToListAsync();

            var entityListDto = entityList.MapTo<List<DictionaryItemListDto>>();

            return new ListResultDto<DictionaryItemListDto>
            {
                Items = entityListDto
            };
        }

        /// <summary>
        /// 根据id获取单条数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(DictionaryItemPermissions.Node)]
        public async Task<DictionaryItemListDto> GetById(EntityDto input)
        {
            var entity = await _repository.GetAsync(input.Id);
            return entity.MapTo<DictionaryItemListDto>();
        }

        /// <summary>
        /// 获取用于更新的View数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(DictionaryItemPermissions.Create, DictionaryItemPermissions.Edit)]
        public async Task<GetDictionaryItemForEditOutput> GetForEdit(NullableIdDto input)
        {
            var output = new GetDictionaryItemForEditOutput();
            DictionaryItemEditDto editDto;
            if (input.Id.HasValue)
            {
                var entity = await _repository.GetAsync(input.Id.Value);
                editDto = entity.MapTo<DictionaryItemEditDto>();
                var values = _dictionaryValueRepository.GetAsync(entity.Id);
                output.DictionaryValues = values.MapTo<List<DictionaryValueListDto>>();
            }
            else
            {
                editDto = new DictionaryItemEditDto();
            }

            output.DictionaryItem = editDto;

            return output;
        }

        /// <summary>
        /// 新建或修改
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(DictionaryItemPermissions.Create, DictionaryItemPermissions.Edit)]
        public async Task<DictionaryItemListDto> CreateOrUpdate(CreateOrUpdateDictionaryItemInput input)
        {
            if (input.DictionaryItem.Id.HasValue)
            {
                return await Update(input);
            }
            else
            {
                return await Create(input);
            }
        }

        /// <summary>
        /// 新建
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(DictionaryItemPermissions.Create)]
        public async Task<DictionaryItemListDto> Create(CreateOrUpdateDictionaryItemInput input)
        {
            var entity = input.DictionaryItem.MapTo<DictionaryItem>();
            return (await _repository.InsertAsync(entity)).MapTo<DictionaryItemListDto>();
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(DictionaryItemPermissions.Edit)]
        public async Task<DictionaryItemListDto> Update(CreateOrUpdateDictionaryItemInput input)
        {
            var entity = input.DictionaryItem.MapTo<DictionaryItem>();
            return (await _repository.UpdateAsync(entity)).MapTo<DictionaryItemListDto>();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(DictionaryItemPermissions.Delete)]
        public async Task Delete(EntityDto input)
        {
            await _repository.DeleteAsync(input.Id);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(DictionaryItemPermissions.BatchDelete)]
        public async Task BatchDelete(List<long> input)
        {
            await _repository.DeleteAsync(a => input.Contains(a.Id));
        }
    }
}
