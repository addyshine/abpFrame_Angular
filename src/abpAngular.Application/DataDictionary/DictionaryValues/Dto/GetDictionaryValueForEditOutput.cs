﻿using System;
using System.Collections.Generic;
using System.Text;

namespace abpAngular.DataDictionary.DictionaryValues.Dto
{
    public class GetDictionaryValueForEditOutput
    {
        public DictionaryValueEditDto DictionaryValue { get; set; }
    }
}
