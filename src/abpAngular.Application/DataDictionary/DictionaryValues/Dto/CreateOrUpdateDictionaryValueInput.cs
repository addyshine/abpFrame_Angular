﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace abpAngular.DataDictionary.DictionaryValues.Dto
{
    public class CreateOrUpdateDictionaryValueInput
    {
        [Required]
        public DictionaryValueEditDto DictionaryValue { get; set; }
    }
}
