﻿using Abp.Runtime.Validation;
using abpAngular.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Extensions;

namespace abpAngular.DataDictionary.DictionaryValues.Dto
{
    public class GetDictionaryValuesInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public int? DictionaryItemId { get; set; }

        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "SortCode ASC";
            }
        }
    }
}
