﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace abpAngular.DataDictionary.DictionaryValues.Dto
{
    public class DictionaryValueEditDto
    {
        public int? Id { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "数据名称超出最大长度")]
        public string Name { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "数据值超出最大长度")]
        public string DValue { get; set; }

        public int DictionaryItemId { get; set; }

        public int SortCode { get; set; }
    }
}
