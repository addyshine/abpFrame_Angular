﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace abpAngular.DataDictionary.DictionaryValues.Dto
{
    public class DictionaryValueListDto: EntityDto
    {
        public string Name { get; set; }
        public string DValue { get; set; }

        public int DictionaryItemId { get; set; }
    }
}
