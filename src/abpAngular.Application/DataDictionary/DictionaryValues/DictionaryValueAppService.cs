﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using abpAngular.DataDictionary.DictionaryValues.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using Abp.Linq.Extensions;
using abpAngular.Authorization;
using Abp.Collections.Extensions;
using Abp.Extensions;

namespace abpAngular.DataDictionary.DictionaryValues
{
    /// <summary>
    /// 数据字典值服务
    /// </summary>
    public class DictionaryValueAppService : AbpFrameAppServiceBase, IDictionaryValueAppService
    {
        private readonly IRepository<DictionaryValue> _repository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="repository"></param>
        public DictionaryValueAppService(
            IRepository<DictionaryValue> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns></returns>
        [AbpAuthorize(DictionaryValuePermissions.Node)]
        public async Task<ListResultDto<DictionaryValueListDto>> GetAll()
        {
            var query = _repository.GetAll();
            var count = await query.CountAsync();

            var entityList = await query.OrderBy(t => t.SortCode).ToListAsync();

            var entityListDto = entityList.MapTo<List<DictionaryValueListDto>>();

            return new ListResultDto<DictionaryValueListDto>
            {
                Items = entityListDto
            };
        }

        /// <summary>
        /// 获取分页列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(DictionaryValuePermissions.Node)]
        public async Task<PagedResultDto<DictionaryValueListDto>> GetPageData(GetDictionaryValuesInput input)
        {

            var query = CreateDictionaryValueQuery(input);

            var count = await query.CountAsync();

            var entityList = await query
                .OrderBy(input.Sorting).AsNoTracking()
                .PageBy(input)
                .ToListAsync();

            var entityListDtos = entityList.MapTo<List<DictionaryValueListDto>>();

            return new PagedResultDto<DictionaryValueListDto>(count, entityListDtos);
        }

        /// <summary>
        /// 拼接查询条件
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private IQueryable<DictionaryValue> CreateDictionaryValueQuery(GetDictionaryValuesInput input)
        {
            var query = _repository.GetAll();

            query = query.WhereIf(!input.Filter.IsNullOrEmpty(),
                p => p.Name.Contains(input.Filter) || p.DValue.Contains(input.Filter));

            query = query.WhereIf(input.DictionaryItemId.HasValue, p => p.DictionaryItemId == input.DictionaryItemId);

            return query;
        }

        /// <summary>
        /// 根据id获取单条数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(DictionaryValuePermissions.Node)]
        public async Task<DictionaryValueListDto> GetById(EntityDto input)
        {
            var entity = await _repository.GetAsync(input.Id);
            return entity.MapTo<DictionaryValueListDto>();
        }

        /// <summary>
        /// 获取用于更新的View数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(DictionaryValuePermissions.Create, DictionaryValuePermissions.Edit)]
        public async Task<GetDictionaryValueForEditOutput> GetForEdit(NullableIdDto input)
        {
            var output = new GetDictionaryValueForEditOutput();
            DictionaryValueEditDto editDto;
            if (input.Id.HasValue)
            {
                var entity = await _repository.GetAsync(input.Id.Value);
                editDto = entity.MapTo<DictionaryValueEditDto>();
            }
            else
            {
                editDto = new DictionaryValueEditDto();
            }

            output.DictionaryValue = editDto;

            return output;
        }

        /// <summary>
        /// 新建或修改
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(DictionaryValuePermissions.Create, DictionaryValuePermissions.Edit)]
        public async Task<DictionaryValueListDto> CreateOrUpdate(CreateOrUpdateDictionaryValueInput input)
        {
            if (input.DictionaryValue.Id.HasValue)
            {
                return await Update(input);
            }
            else
            {
                return await Create(input);
            }
        }

        /// <summary>
        /// 新建
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(DictionaryValuePermissions.Create)]
        public async Task<DictionaryValueListDto> Create(CreateOrUpdateDictionaryValueInput input)
        {
            var entity = input.DictionaryValue.MapTo<DictionaryValue>();
            return (await _repository.InsertAsync(entity)).MapTo<DictionaryValueListDto>();
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(DictionaryValuePermissions.Edit)]
        public async Task<DictionaryValueListDto> Update(CreateOrUpdateDictionaryValueInput input)
        {
            var entity = input.DictionaryValue.MapTo<DictionaryValue>();
            return (await _repository.UpdateAsync(entity)).MapTo<DictionaryValueListDto>();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(DictionaryValuePermissions.Delete)]
        public async Task Delete(EntityDto input)
        {
            await _repository.DeleteAsync(input.Id);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(DictionaryValuePermissions.BatchDelete)]
        public async Task BatchDelete(List<long> input)
        {
            await _repository.DeleteAsync(a => input.Contains(a.Id));
        }
    }
}
