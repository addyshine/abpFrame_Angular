﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using abpAngular.DataDictionary.DictionaryValues.Dto;

namespace abpAngular.DataDictionary.DictionaryValues
{
    /// <summary>
    /// 数据字典值服务接口
    /// </summary>
    public interface IDictionaryValueAppService: IApplicationService
    {
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns></returns>
        Task<ListResultDto<DictionaryValueListDto>> GetAll();

        /// <summary>
        /// 获取分页列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PagedResultDto<DictionaryValueListDto>> GetPageData(GetDictionaryValuesInput input);

        /// <summary>
        /// 根据id获取单条数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<DictionaryValueListDto> GetById(EntityDto input);

        /// <summary>
        /// 获取用于更新的View数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<GetDictionaryValueForEditOutput> GetForEdit(NullableIdDto input);

        /// <summary>
        /// 新建或编辑
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<DictionaryValueListDto> CreateOrUpdate(CreateOrUpdateDictionaryValueInput input);

        /// <summary>
        /// 新建
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<DictionaryValueListDto> Create(CreateOrUpdateDictionaryValueInput input);

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<DictionaryValueListDto> Update(CreateOrUpdateDictionaryValueInput input);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task Delete(EntityDto input);

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task BatchDelete(List<long> input);
    }
}
