﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using abpAngular.Authorization;

namespace abpAngular
{
    [DependsOn(
        typeof(abpAngularCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class abpAngularApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<AppAuthorizationProvider>();

            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.CreateMappings);
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(abpAngularApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
