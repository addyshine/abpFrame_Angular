﻿using System.Threading.Tasks;
using Abp.Application.Services;
using abpAngular.Configuration.Tenants.Dto;

namespace abpAngular.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);

        Task ClearLogo();

        Task ClearCustomCss();
    }
}
