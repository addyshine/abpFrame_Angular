﻿using System.Threading.Tasks;
using Abp.Application.Services;
using abpAngular.Configuration.Host.Dto;

namespace abpAngular.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);

        Task SendTestEmail(SendTestEmailInput input);
    }
}
