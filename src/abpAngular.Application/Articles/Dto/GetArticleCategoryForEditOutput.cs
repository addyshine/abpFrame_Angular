﻿using System.ComponentModel.DataAnnotations;

namespace abpAngular.Articles.Dto
{
    public class GetArticleCategoryForEditOutput
    {
        public ArticleCategoryEditDto ArticleCategory { get; set; }
    }
}
