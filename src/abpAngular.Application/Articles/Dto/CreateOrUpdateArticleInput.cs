﻿using System.ComponentModel.DataAnnotations;

namespace abpAngular.Articles.Dto
{
    public class CreateOrUpdateArticleInput
    {
        public ArticleEditDto Article { get; set; }
    }
}
