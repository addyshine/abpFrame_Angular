﻿using System.ComponentModel.DataAnnotations;

namespace abpAngular.Articles.Dto
{
    public class GetArticleForEditOutput
    {
        public ArticleEditDto Article { get; set; }
    }
}
