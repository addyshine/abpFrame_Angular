﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace abpAngular.Articles.Dto
{
    public class ArticleCategoryEditDto:EntityDto<long?>
    {
        [Display(Name = "类别名称")]
        [Required]
        [MaxLength(50, ErrorMessage = "类别名称超出最大长度")]
        public string CategoryName { get; set; }
        [Display(Name = "备注")]
        [MaxLength(100, ErrorMessage = "备注超出最大长度")]
        public string Remark { get; set; }
    }
}
