﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Abp.AutoMapper;

namespace abpAngular.Articles.Dto
{
    [AutoMapFrom(typeof(Article))]
    public class ArticleListDto
    {
        public long Id { get; set; }

         [Display(Name = "文章标题")]
         [Required]
         [MaxLength(50, ErrorMessage = "文章标题超出最大长度")]
         public string Title { get; set; }
         [Display(Name = "文章内容")]
         public string Content { get; set; }
    }
}
