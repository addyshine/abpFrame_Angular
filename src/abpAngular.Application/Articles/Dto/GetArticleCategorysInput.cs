﻿using Abp.Extensions;
using Abp.Runtime.Validation;
using abpAngular.Dto;

namespace abpAngular.Articles.Dto
{
    public class GetArticleCategorysInput: PagedAndSortedInputDto, IShouldNormalize
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Filter { get; set; }

        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
				Sorting = "CreationTime DESC";
            }
        }
    }
}
