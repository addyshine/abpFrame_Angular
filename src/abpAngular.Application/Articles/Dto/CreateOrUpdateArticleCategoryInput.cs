﻿using System.ComponentModel.DataAnnotations;

namespace abpAngular.Articles.Dto
{
    public class CreateOrUpdateArticleCategoryInput
    {
        public ArticleCategoryEditDto ArticleCategory { get; set; }
    }
}
