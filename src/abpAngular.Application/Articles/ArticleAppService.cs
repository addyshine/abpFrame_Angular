﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using abpAngular.Articles.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using Abp.Linq.Extensions;
using abpAngular.Authorization;
using Abp.Collections.Extensions;
using Abp.Extensions;

namespace abpAngular.Articles
{
    /// <summary>
    /// 文章模块服务
    /// </summary>
    [AbpAuthorize(ArticlePermissions.Node)]
    public class ArticleAppService : AbpFrameAppServiceBase, IArticleAppService
    {
        private readonly IRepository<Article, long> _repository;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="repository"></param>
        public ArticleAppService(IRepository<Article, long> repository)
        {
            _repository = repository;
        }
    
        /// <summary>
        /// 拼接查询条件
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private IQueryable<Article> CreateArticleQuery(GetArticlesInput input)
        {
            var query = _repository.GetAll();
    
            //此处写自己的查询条件
            //query = query.WhereIf(!input.Filter.IsNullOrEmpty(),
            //p => p.Name.Contains(input.Filter) || p.DValue.Contains(input.Filter));

            //query = query.WhereIf(input.DictionaryItemId.HasValue, p => p.DictionaryItemId == input.DictionaryItemId);

            return query;
        }

        /// <summary>
        /// 获取更新文章模块的数据
        /// </summary>
        [AbpAuthorize(ArticlePermissions.Node)]
        public async Task<PagedResultDto<ArticleListDto>> GetArticles(GetArticlesInput input)
        {
            var query = CreateArticleQuery(input);

            var count = await query.CountAsync();

            var entityList = await query
                .OrderBy(input.Sorting).AsNoTracking()
                .PageBy(input)
                .ToListAsync();

            var entityListDtos = entityList.MapTo<List<ArticleListDto>>();

            return new PagedResultDto<ArticleListDto>(count, entityListDtos);
        }

        /// <summary>
        /// 获取更新文章模块的数据
        /// </summary>
        [AbpAuthorize(ArticlePermissions.Create, ArticlePermissions.Edit)]
        public async Task<GetArticleForEditOutput> GetArticleForEdit(NullableIdDto<long> input)
        {
            var output = new GetArticleForEditOutput();
            ArticleEditDto editDto;
            if (input.Id.HasValue)
            {
                var entity = await _repository.GetAsync(input.Id.Value);
                editDto = entity.MapTo<ArticleEditDto>();
            }
            else
            {
                editDto = new ArticleEditDto();
            }

            output.Article = editDto;

            return output;
        }

        /// <summary>
        /// 创建或编辑文章模块
        /// </summary>
        [AbpAuthorize(ArticlePermissions.Create, ArticlePermissions.Edit)]
        public async Task CreateOrUpdateArticle(CreateOrUpdateArticleInput input)
        {
            if (!input.Article.Id.HasValue)
            {
                await CreateArticleAsync(input);
            }
            else
            {
                await UpdateArticleAsync(input);
            }
        }

        /// <summary>
        /// 新建
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(ArticlePermissions.Create)]
        public async Task<ArticleListDto> CreateArticleAsync(CreateOrUpdateArticleInput input)
        {
            var entity = input.Article.MapTo<Article>();
            return (await _repository.InsertAsync(entity)).MapTo<ArticleListDto>();
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(ArticlePermissions.Edit)]
        public async Task<ArticleListDto> UpdateArticleAsync(CreateOrUpdateArticleInput input)
        {
            var entity = input.Article.MapTo<Article>();
            return (await _repository.UpdateAsync(entity)).MapTo<ArticleListDto>();
        }

        /// <summary>
        /// 删除文章模块
        /// </summary>
        [AbpAuthorize(ArticlePermissions.Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            await _repository.DeleteAsync(input.Id);
        }

        /// <summary>
        /// 批量删除文章模块
        /// </summary>
        [AbpAuthorize(ArticlePermissions.BatchDelete)]
        public async Task BatchDelete(List<long> input)
        {
            await _repository.DeleteAsync(a => input.Contains(a.Id));
        }
    }
}
