﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using abpAngular.Articles.Dto;

namespace abpAngular.Articles
{
    /// <summary>
    /// 文章类别服务接口
    /// </summary>
    public interface IArticleCategoryAppService: IApplicationService
    {
        /// <summary>
        /// 获取文章类别列表
        /// </summary>
        Task<PagedResultDto<ArticleCategoryListDto>> GetArticleCategorys(GetArticleCategorysInput input);

        /// <summary>
        /// 获取文章类别
        /// </summary>
        Task<GetArticleCategoryForEditOutput> GetArticleCategoryForEdit(NullableIdDto<long> input);

        /// <summary>
        /// 创建或编辑文章类别
        /// </summary>
        Task CreateOrUpdateArticleCategory(CreateOrUpdateArticleCategoryInput input);

        /// <summary>
        /// 删除文章类别
        /// </summary>
        Task Delete(EntityDto<long> input);

        /// <summary>
        /// 删除文章类别
        /// </summary>
        Task BatchDelete(List<long> input);
    }
}
