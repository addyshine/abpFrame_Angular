﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using abpAngular.Articles.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using Abp.Linq.Extensions;
using abpAngular.Authorization;
using Abp.Collections.Extensions;
using Abp.Extensions;

namespace abpAngular.Articles
{
    /// <summary>
    /// 文章类别服务
    /// </summary>
    [AbpAuthorize(ArticleCategoryPermissions.Node)]
    public class ArticleCategoryAppService : AbpFrameAppServiceBase, IArticleCategoryAppService
    {
        private readonly IRepository<ArticleCategory, long> _repository;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="repository"></param>
        public ArticleCategoryAppService(IRepository<ArticleCategory, long> repository)
        {
            _repository = repository;
        }
    
        /// <summary>
        /// 拼接查询条件
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private IQueryable<ArticleCategory> CreateArticleCategoryQuery(GetArticleCategorysInput input)
        {
            var query = _repository.GetAll();

            //此处写自己的查询条件
            query = query.WhereIf(!input.Filter.IsNullOrEmpty(),
            p => p.CategoryName.Contains(input.Filter));

            return query;
        }

        /// <summary>
        /// 获取更新文章类别的数据
        /// </summary>
        [AbpAuthorize(ArticleCategoryPermissions.Node)]
        public async Task<PagedResultDto<ArticleCategoryListDto>> GetArticleCategorys(GetArticleCategorysInput input)
        {
            var query = CreateArticleCategoryQuery(input);

            var count = await query.CountAsync();

            var entityList = await query
                .OrderBy(input.Sorting).AsNoTracking()
                .PageBy(input)
                .ToListAsync();

            var entityListDtos = entityList.MapTo<List<ArticleCategoryListDto>>();

            return new PagedResultDto<ArticleCategoryListDto>(count, entityListDtos);
        }

        /// <summary>
        /// 获取更新文章类别的数据
        /// </summary>
        [AbpAuthorize(ArticleCategoryPermissions.Create, ArticleCategoryPermissions.Edit)]
        public async Task<GetArticleCategoryForEditOutput> GetArticleCategoryForEdit(NullableIdDto<long> input)
        {
            var output = new GetArticleCategoryForEditOutput();
            ArticleCategoryEditDto editDto;
            if (input.Id.HasValue)
            {
                var entity = await _repository.GetAsync(input.Id.Value);
                editDto = entity.MapTo<ArticleCategoryEditDto>();
            }
            else
            {
                editDto = new ArticleCategoryEditDto();
            }

            output.ArticleCategory = editDto;

            return output;
        }

        /// <summary>
        /// 创建或编辑文章类别
        /// </summary>
        [AbpAuthorize(ArticleCategoryPermissions.Create, ArticleCategoryPermissions.Edit)]
        public async Task CreateOrUpdateArticleCategory(CreateOrUpdateArticleCategoryInput input)
        {
            if (!input.ArticleCategory.Id.HasValue)
            {
                await CreateArticleCategoryAsync(input);
            }
            else
            {
                await UpdateArticleCategoryAsync(input);
            }
        }

        /// <summary>
        /// 新建
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(ArticleCategoryPermissions.Create)]
        public async Task<ArticleCategoryEditDto> CreateArticleCategoryAsync(CreateOrUpdateArticleCategoryInput input)
        {
            var entity = input.ArticleCategory.MapTo<ArticleCategory>();
            return (await _repository.InsertAsync(entity)).MapTo<ArticleCategoryEditDto>();
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(ArticleCategoryPermissions.Edit)]
        public async Task<ArticleCategoryEditDto> UpdateArticleCategoryAsync(CreateOrUpdateArticleCategoryInput input)
        {
            var entity = input.ArticleCategory.MapTo<ArticleCategory>();
            return (await _repository.UpdateAsync(entity)).MapTo<ArticleCategoryEditDto>();
        }

        /// <summary>
        /// 删除文章类别
        /// </summary>
        [AbpAuthorize(ArticleCategoryPermissions.Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            await _repository.DeleteAsync(input.Id);
        }

        /// <summary>
        /// 批量删除文章类别
        /// </summary>
        [AbpAuthorize(ArticleCategoryPermissions.BatchDelete)]
        public async Task BatchDelete(List<long> input)
        {
            await _repository.DeleteAsync(a => input.Contains(a.Id));
        }
    }
}
