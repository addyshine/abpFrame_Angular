﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using abpAngular.Articles.Dto;

namespace abpAngular.Articles
{
    /// <summary>
    /// 文章模块服务接口
    /// </summary>
    public interface IArticleAppService: IApplicationService
    {
        /// <summary>
        /// 获取文章模块列表
        /// </summary>
        Task<PagedResultDto<ArticleListDto>> GetArticles(GetArticlesInput input);

        /// <summary>
        /// 获取文章模块
        /// </summary>
        Task<GetArticleForEditOutput> GetArticleForEdit(NullableIdDto<long> input);

        /// <summary>
        /// 创建或编辑文章模块
        /// </summary>
        Task CreateOrUpdateArticle(CreateOrUpdateArticleInput input);

        /// <summary>
        /// 删除文章模块
        /// </summary>
        Task Delete(EntityDto<long> input);

        /// <summary>
        /// 删除文章模块
        /// </summary>
        Task BatchDelete(List<long> input);
    }
}
