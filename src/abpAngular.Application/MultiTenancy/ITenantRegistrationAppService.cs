using System.Threading.Tasks;
using Abp.Application.Services;
using abpAngular.Editions.Dto;
using abpAngular.MultiTenancy.Dto;

namespace abpAngular.MultiTenancy
{
    public interface ITenantRegistrationAppService: IApplicationService
    {
        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}