﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using abpAngular.MultiTenancy.HostDashboard.Dto;

namespace abpAngular.MultiTenancy.HostDashboard
{
    public interface IIncomeStatisticsService
    {
        Task<List<IncomeStastistic>> GetIncomeStatisticsData(DateTime startDate, DateTime endDate,
            ChartDateInterval dateInterval);
    }
}