﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using abpAngular.MultiTenancy.Accounting.Dto;

namespace abpAngular.MultiTenancy.Accounting
{
    public interface IInvoiceAppService
    {
        Task<InvoiceDto> GetInvoiceInfo(EntityDto<long> input);

        Task CreateInvoice(CreateInvoiceDto input);
    }
}
