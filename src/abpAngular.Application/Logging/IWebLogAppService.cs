﻿using Abp.Application.Services;
using abpAngular.Dto;
using abpAngular.Logging.Dto;

namespace abpAngular.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}
