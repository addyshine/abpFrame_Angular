﻿using System.ComponentModel.DataAnnotations;

namespace abpAngular.Authorization.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}
