﻿using System.Collections.Generic;
using abpAngular.Authorization.Permissions.Dto;

namespace abpAngular.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}