using System.Collections.Generic;
using abpAngular.Authorization.Users.Dto;
using abpAngular.Dto;

namespace abpAngular.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}