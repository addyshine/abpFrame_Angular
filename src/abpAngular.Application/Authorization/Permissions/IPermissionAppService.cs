﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using abpAngular.Authorization.Permissions.Dto;

namespace abpAngular.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
