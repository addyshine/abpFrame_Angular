﻿using System.Threading.Tasks;
using Abp.Application.Services;
using abpAngular.Sessions.Dto;

namespace abpAngular.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();

        Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken();
    }
}
