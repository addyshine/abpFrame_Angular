﻿using System.ComponentModel.DataAnnotations;

namespace abpAngular.Localization.Dto
{
    public class CreateOrUpdateLanguageInput
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}