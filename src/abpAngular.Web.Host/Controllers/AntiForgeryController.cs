﻿using Microsoft.AspNetCore.Antiforgery;

namespace abpAngular.Web.Controllers
{
    public class AntiForgeryController : AbpFrameControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
