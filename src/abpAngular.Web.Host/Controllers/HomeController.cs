﻿using Abp.Auditing;
using Microsoft.AspNetCore.Mvc;

namespace abpAngular.Web.Controllers
{
    public class HomeController : AbpFrameControllerBase
    {
        [DisableAuditing]
        public IActionResult Index()
        {
            return Redirect("/swagger");
        }
    }
}
