﻿using Abp.AspNetCore.Mvc.Authorization;
using abpAngular.Storage;

namespace abpAngular.Web.Controllers
{
    [AbpMvcAuthorize]
    public class ProfileController : ProfileControllerBase
    {
        public ProfileController(ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
        }
    }
}