﻿using abpAngular.MultiTenancy.Payments;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace abpAngular.EntityMapper.SubscriptionPayments
{
    public class SubscriptionPaymentCfg : IEntityTypeConfiguration<SubscriptionPayment>
    {
        public void Configure(EntityTypeBuilder<SubscriptionPayment> builder)
        {
            builder.ToTable(AbpFrameConsts.DbTablePrefix.Base + "SubscriptionPayments");
        }
    }
}
