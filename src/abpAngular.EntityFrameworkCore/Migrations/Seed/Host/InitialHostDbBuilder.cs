﻿using abpAngular.EntityFrameworkCore;

namespace abpAngular.Migrations.Seed.Host
{
    public class InitialHostDbBuilder
    {
        private readonly AbpFrameDbContext _context;

        public InitialHostDbBuilder(AbpFrameDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();

            _context.SaveChanges();
        }
    }
}
