﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace abpAngular.Migrations
{
    public partial class updatePre0322 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Base.EntityChanges_Base.EntityChangeSets_EntityChangeSetId",
                table: "Base.EntityChanges");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.EntityPropertyChanges_Base.EntityChanges_EntityChangeId",
                table: "Base.EntityPropertyChanges");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.Features_Base.Editions_EditionId",
                table: "Base.Features");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.OrganizationUnits_Base.OrganizationUnits_ParentId",
                table: "Base.OrganizationUnits");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.Permissions_Base.Roles_RoleId",
                table: "Base.Permissions");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.Permissions_Base.Users_UserId",
                table: "Base.Permissions");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.RoleClaims_Base.Roles_RoleId",
                table: "Base.RoleClaims");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.Roles_Base.Users_CreatorUserId",
                table: "Base.Roles");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.Roles_Base.Users_DeleterUserId",
                table: "Base.Roles");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.Roles_Base.Users_LastModifierUserId",
                table: "Base.Roles");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.Settings_Base.Users_UserId",
                table: "Base.Settings");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.SubscriptionPayments_Base.Editions_EditionId",
                table: "Base.SubscriptionPayments");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.Tenants_Base.Users_CreatorUserId",
                table: "Base.Tenants");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.Tenants_Base.Users_DeleterUserId",
                table: "Base.Tenants");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.Tenants_Base.Editions_EditionId",
                table: "Base.Tenants");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.Tenants_Base.Users_LastModifierUserId",
                table: "Base.Tenants");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.UserClaims_Base.Users_UserId",
                table: "Base.UserClaims");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.UserLogins_Base.Users_UserId",
                table: "Base.UserLogins");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.UserRoles_Base.Users_UserId",
                table: "Base.UserRoles");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.Users_Base.Users_CreatorUserId",
                table: "Base.Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.Users_Base.Users_DeleterUserId",
                table: "Base.Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.Users_Base.Users_LastModifierUserId",
                table: "Base.Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Base.UserTokens_Base.Users_UserId",
                table: "Base.UserTokens");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.UserTokens",
                table: "Base.UserTokens");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.Users",
                table: "Base.Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.UserRoles",
                table: "Base.UserRoles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.UserOrganizationUnits",
                table: "Base.UserOrganizationUnits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.UserNotifications",
                table: "Base.UserNotifications");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.UserLogins",
                table: "Base.UserLogins");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.UserLoginAttempts",
                table: "Base.UserLoginAttempts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.UserClaims",
                table: "Base.UserClaims");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.UserAccounts",
                table: "Base.UserAccounts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.Tenants",
                table: "Base.Tenants");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.TenantNotifications",
                table: "Base.TenantNotifications");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.SubscriptionPayments",
                table: "Base.SubscriptionPayments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.Settings",
                table: "Base.Settings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.Roles",
                table: "Base.Roles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.RoleClaims",
                table: "Base.RoleClaims");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.PersistedGrants",
                table: "Base.PersistedGrants");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.Permissions",
                table: "Base.Permissions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.OrganizationUnits",
                table: "Base.OrganizationUnits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.OrganizationUnitRoles",
                table: "Base.OrganizationUnitRoles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.NotificationSubscriptions",
                table: "Base.NotificationSubscriptions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.Notifications",
                table: "Base.Notifications");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.LanguageTexts",
                table: "Base.LanguageTexts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.Languages",
                table: "Base.Languages");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.Features",
                table: "Base.Features");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.EntityPropertyChanges",
                table: "Base.EntityPropertyChanges");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.EntityChangeSets",
                table: "Base.EntityChangeSets");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.EntityChanges",
                table: "Base.EntityChanges");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.Editions",
                table: "Base.Editions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.BackgroundJobs",
                table: "Base.BackgroundJobs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base.AuditLogs",
                table: "Base.AuditLogs");

            migrationBuilder.RenameTable(
                name: "Base.UserTokens",
                newName: "Base_UserTokens");

            migrationBuilder.RenameTable(
                name: "Base.Users",
                newName: "Base_Users");

            migrationBuilder.RenameTable(
                name: "Base.UserRoles",
                newName: "Base_UserRoles");

            migrationBuilder.RenameTable(
                name: "Base.UserOrganizationUnits",
                newName: "Base_UserOrganizationUnits");

            migrationBuilder.RenameTable(
                name: "Base.UserNotifications",
                newName: "Base_UserNotifications");

            migrationBuilder.RenameTable(
                name: "Base.UserLogins",
                newName: "Base_UserLogins");

            migrationBuilder.RenameTable(
                name: "Base.UserLoginAttempts",
                newName: "Base_UserLoginAttempts");

            migrationBuilder.RenameTable(
                name: "Base.UserClaims",
                newName: "Base_UserClaims");

            migrationBuilder.RenameTable(
                name: "Base.UserAccounts",
                newName: "Base_UserAccounts");

            migrationBuilder.RenameTable(
                name: "Base.Tenants",
                newName: "Base_Tenants");

            migrationBuilder.RenameTable(
                name: "Base.TenantNotifications",
                newName: "Base_TenantNotifications");

            migrationBuilder.RenameTable(
                name: "Base.SubscriptionPayments",
                newName: "Base_SubscriptionPayments");

            migrationBuilder.RenameTable(
                name: "Base.Settings",
                newName: "Base_Settings");

            migrationBuilder.RenameTable(
                name: "Base.Roles",
                newName: "Base_Roles");

            migrationBuilder.RenameTable(
                name: "Base.RoleClaims",
                newName: "Base_RoleClaims");

            migrationBuilder.RenameTable(
                name: "Base.PersistedGrants",
                newName: "Base_PersistedGrants");

            migrationBuilder.RenameTable(
                name: "Base.Permissions",
                newName: "Base_Permissions");

            migrationBuilder.RenameTable(
                name: "Base.OrganizationUnits",
                newName: "Base_OrganizationUnits");

            migrationBuilder.RenameTable(
                name: "Base.OrganizationUnitRoles",
                newName: "Base_OrganizationUnitRoles");

            migrationBuilder.RenameTable(
                name: "Base.NotificationSubscriptions",
                newName: "Base_NotificationSubscriptions");

            migrationBuilder.RenameTable(
                name: "Base.Notifications",
                newName: "Base_Notifications");

            migrationBuilder.RenameTable(
                name: "Base.LanguageTexts",
                newName: "Base_LanguageTexts");

            migrationBuilder.RenameTable(
                name: "Base.Languages",
                newName: "Base_Languages");

            migrationBuilder.RenameTable(
                name: "Base.Features",
                newName: "Base_Features");

            migrationBuilder.RenameTable(
                name: "Base.EntityPropertyChanges",
                newName: "Base_EntityPropertyChanges");

            migrationBuilder.RenameTable(
                name: "Base.EntityChangeSets",
                newName: "Base_EntityChangeSets");

            migrationBuilder.RenameTable(
                name: "Base.EntityChanges",
                newName: "Base_EntityChanges");

            migrationBuilder.RenameTable(
                name: "Base.Editions",
                newName: "Base_Editions");

            migrationBuilder.RenameTable(
                name: "Base.BackgroundJobs",
                newName: "Base_BackgroundJobs");

            migrationBuilder.RenameTable(
                name: "Base.AuditLogs",
                newName: "Base_AuditLogs");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserTokens_TenantId_UserId",
                table: "Base_UserTokens",
                newName: "IX_Base_UserTokens_TenantId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserTokens_UserId",
                table: "Base_UserTokens",
                newName: "IX_Base_UserTokens_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Users_TenantId_NormalizedUserName",
                table: "Base_Users",
                newName: "IX_Base_Users_TenantId_NormalizedUserName");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Users_TenantId_NormalizedEmailAddress",
                table: "Base_Users",
                newName: "IX_Base_Users_TenantId_NormalizedEmailAddress");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Users_LastModifierUserId",
                table: "Base_Users",
                newName: "IX_Base_Users_LastModifierUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Users_DeleterUserId",
                table: "Base_Users",
                newName: "IX_Base_Users_DeleterUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Users_CreatorUserId",
                table: "Base_Users",
                newName: "IX_Base_Users_CreatorUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserRoles_TenantId_UserId",
                table: "Base_UserRoles",
                newName: "IX_Base_UserRoles_TenantId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserRoles_TenantId_RoleId",
                table: "Base_UserRoles",
                newName: "IX_Base_UserRoles_TenantId_RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserRoles_UserId",
                table: "Base_UserRoles",
                newName: "IX_Base_UserRoles_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserOrganizationUnits_TenantId_UserId",
                table: "Base_UserOrganizationUnits",
                newName: "IX_Base_UserOrganizationUnits_TenantId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserOrganizationUnits_TenantId_OrganizationUnitId",
                table: "Base_UserOrganizationUnits",
                newName: "IX_Base_UserOrganizationUnits_TenantId_OrganizationUnitId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserNotifications_UserId_State_CreationTime",
                table: "Base_UserNotifications",
                newName: "IX_Base_UserNotifications_UserId_State_CreationTime");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserLogins_TenantId_LoginProvider_ProviderKey",
                table: "Base_UserLogins",
                newName: "IX_Base_UserLogins_TenantId_LoginProvider_ProviderKey");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserLogins_TenantId_UserId",
                table: "Base_UserLogins",
                newName: "IX_Base_UserLogins_TenantId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserLogins_UserId",
                table: "Base_UserLogins",
                newName: "IX_Base_UserLogins_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserLoginAttempts_TenancyName_UserNameOrEmailAddress_Result",
                table: "Base_UserLoginAttempts",
                newName: "IX_Base_UserLoginAttempts_TenancyName_UserNameOrEmailAddress_Result");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserLoginAttempts_UserId_TenantId",
                table: "Base_UserLoginAttempts",
                newName: "IX_Base_UserLoginAttempts_UserId_TenantId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserClaims_TenantId_ClaimType",
                table: "Base_UserClaims",
                newName: "IX_Base_UserClaims_TenantId_ClaimType");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserClaims_UserId",
                table: "Base_UserClaims",
                newName: "IX_Base_UserClaims_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserAccounts_TenantId_UserName",
                table: "Base_UserAccounts",
                newName: "IX_Base_UserAccounts_TenantId_UserName");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserAccounts_TenantId_UserId",
                table: "Base_UserAccounts",
                newName: "IX_Base_UserAccounts_TenantId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserAccounts_TenantId_EmailAddress",
                table: "Base_UserAccounts",
                newName: "IX_Base_UserAccounts_TenantId_EmailAddress");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserAccounts_UserName",
                table: "Base_UserAccounts",
                newName: "IX_Base_UserAccounts_UserName");

            migrationBuilder.RenameIndex(
                name: "IX_Base.UserAccounts_EmailAddress",
                table: "Base_UserAccounts",
                newName: "IX_Base_UserAccounts_EmailAddress");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Tenants_TenancyName",
                table: "Base_Tenants",
                newName: "IX_Base_Tenants_TenancyName");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Tenants_SubscriptionEndDateUtc",
                table: "Base_Tenants",
                newName: "IX_Base_Tenants_SubscriptionEndDateUtc");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Tenants_LastModifierUserId",
                table: "Base_Tenants",
                newName: "IX_Base_Tenants_LastModifierUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Tenants_EditionId",
                table: "Base_Tenants",
                newName: "IX_Base_Tenants_EditionId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Tenants_DeleterUserId",
                table: "Base_Tenants",
                newName: "IX_Base_Tenants_DeleterUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Tenants_CreatorUserId",
                table: "Base_Tenants",
                newName: "IX_Base_Tenants_CreatorUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Tenants_CreationTime",
                table: "Base_Tenants",
                newName: "IX_Base_Tenants_CreationTime");

            migrationBuilder.RenameIndex(
                name: "IX_Base.TenantNotifications_TenantId",
                table: "Base_TenantNotifications",
                newName: "IX_Base_TenantNotifications_TenantId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.SubscriptionPayments_Status_CreationTime",
                table: "Base_SubscriptionPayments",
                newName: "IX_Base_SubscriptionPayments_Status_CreationTime");

            migrationBuilder.RenameIndex(
                name: "IX_Base.SubscriptionPayments_ExternalPaymentId_Gateway",
                table: "Base_SubscriptionPayments",
                newName: "IX_Base_SubscriptionPayments_ExternalPaymentId_Gateway");

            migrationBuilder.RenameIndex(
                name: "IX_Base.SubscriptionPayments_EditionId",
                table: "Base_SubscriptionPayments",
                newName: "IX_Base_SubscriptionPayments_EditionId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Settings_TenantId_Name",
                table: "Base_Settings",
                newName: "IX_Base_Settings_TenantId_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Settings_UserId",
                table: "Base_Settings",
                newName: "IX_Base_Settings_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Roles_TenantId_NormalizedName",
                table: "Base_Roles",
                newName: "IX_Base_Roles_TenantId_NormalizedName");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Roles_LastModifierUserId",
                table: "Base_Roles",
                newName: "IX_Base_Roles_LastModifierUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Roles_DeleterUserId",
                table: "Base_Roles",
                newName: "IX_Base_Roles_DeleterUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Roles_CreatorUserId",
                table: "Base_Roles",
                newName: "IX_Base_Roles_CreatorUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.RoleClaims_TenantId_ClaimType",
                table: "Base_RoleClaims",
                newName: "IX_Base_RoleClaims_TenantId_ClaimType");

            migrationBuilder.RenameIndex(
                name: "IX_Base.RoleClaims_RoleId",
                table: "Base_RoleClaims",
                newName: "IX_Base_RoleClaims_RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.PersistedGrants_SubjectId_ClientId_Type",
                table: "Base_PersistedGrants",
                newName: "IX_Base_PersistedGrants_SubjectId_ClientId_Type");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Permissions_UserId",
                table: "Base_Permissions",
                newName: "IX_Base_Permissions_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Permissions_RoleId",
                table: "Base_Permissions",
                newName: "IX_Base_Permissions_RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Permissions_TenantId_Name",
                table: "Base_Permissions",
                newName: "IX_Base_Permissions_TenantId_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Base.OrganizationUnits_TenantId_Code",
                table: "Base_OrganizationUnits",
                newName: "IX_Base_OrganizationUnits_TenantId_Code");

            migrationBuilder.RenameIndex(
                name: "IX_Base.OrganizationUnits_ParentId",
                table: "Base_OrganizationUnits",
                newName: "IX_Base_OrganizationUnits_ParentId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.OrganizationUnitRoles_TenantId_RoleId",
                table: "Base_OrganizationUnitRoles",
                newName: "IX_Base_OrganizationUnitRoles_TenantId_RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.OrganizationUnitRoles_TenantId_OrganizationUnitId",
                table: "Base_OrganizationUnitRoles",
                newName: "IX_Base_OrganizationUnitRoles_TenantId_OrganizationUnitId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.NotificationSubscriptions_TenantId_NotificationName_EntityTypeName_EntityId_UserId",
                table: "Base_NotificationSubscriptions",
                newName: "IX_Base_NotificationSubscriptions_TenantId_NotificationName_EntityTypeName_EntityId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.NotificationSubscriptions_NotificationName_EntityTypeName_EntityId_UserId",
                table: "Base_NotificationSubscriptions",
                newName: "IX_Base_NotificationSubscriptions_NotificationName_EntityTypeName_EntityId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.LanguageTexts_TenantId_Source_LanguageName_Key",
                table: "Base_LanguageTexts",
                newName: "IX_Base_LanguageTexts_TenantId_Source_LanguageName_Key");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Languages_TenantId_Name",
                table: "Base_Languages",
                newName: "IX_Base_Languages_TenantId_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Features_TenantId_Name",
                table: "Base_Features",
                newName: "IX_Base_Features_TenantId_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Base.Features_EditionId_Name",
                table: "Base_Features",
                newName: "IX_Base_Features_EditionId_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Base.EntityPropertyChanges_EntityChangeId",
                table: "Base_EntityPropertyChanges",
                newName: "IX_Base_EntityPropertyChanges_EntityChangeId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.EntityChangeSets_TenantId_UserId",
                table: "Base_EntityChangeSets",
                newName: "IX_Base_EntityChangeSets_TenantId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.EntityChangeSets_TenantId_Reason",
                table: "Base_EntityChangeSets",
                newName: "IX_Base_EntityChangeSets_TenantId_Reason");

            migrationBuilder.RenameIndex(
                name: "IX_Base.EntityChangeSets_TenantId_CreationTime",
                table: "Base_EntityChangeSets",
                newName: "IX_Base_EntityChangeSets_TenantId_CreationTime");

            migrationBuilder.RenameIndex(
                name: "IX_Base.EntityChanges_EntityTypeFullName_EntityId",
                table: "Base_EntityChanges",
                newName: "IX_Base_EntityChanges_EntityTypeFullName_EntityId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.EntityChanges_EntityChangeSetId",
                table: "Base_EntityChanges",
                newName: "IX_Base_EntityChanges_EntityChangeSetId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.BackgroundJobs_IsAbandoned_NextTryTime",
                table: "Base_BackgroundJobs",
                newName: "IX_Base_BackgroundJobs_IsAbandoned_NextTryTime");

            migrationBuilder.RenameIndex(
                name: "IX_Base.AuditLogs_TenantId_UserId",
                table: "Base_AuditLogs",
                newName: "IX_Base_AuditLogs_TenantId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base.AuditLogs_TenantId_ExecutionTime",
                table: "Base_AuditLogs",
                newName: "IX_Base_AuditLogs_TenantId_ExecutionTime");

            migrationBuilder.RenameIndex(
                name: "IX_Base.AuditLogs_TenantId_ExecutionDuration",
                table: "Base_AuditLogs",
                newName: "IX_Base_AuditLogs_TenantId_ExecutionDuration");

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "Base.DictionaryValue",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "Base.DictionaryItem",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_UserTokens",
                table: "Base_UserTokens",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_Users",
                table: "Base_Users",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_UserRoles",
                table: "Base_UserRoles",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_UserOrganizationUnits",
                table: "Base_UserOrganizationUnits",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_UserNotifications",
                table: "Base_UserNotifications",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_UserLogins",
                table: "Base_UserLogins",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_UserLoginAttempts",
                table: "Base_UserLoginAttempts",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_UserClaims",
                table: "Base_UserClaims",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_UserAccounts",
                table: "Base_UserAccounts",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_Tenants",
                table: "Base_Tenants",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_TenantNotifications",
                table: "Base_TenantNotifications",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_SubscriptionPayments",
                table: "Base_SubscriptionPayments",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_Settings",
                table: "Base_Settings",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_Roles",
                table: "Base_Roles",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_RoleClaims",
                table: "Base_RoleClaims",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_PersistedGrants",
                table: "Base_PersistedGrants",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_Permissions",
                table: "Base_Permissions",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_OrganizationUnits",
                table: "Base_OrganizationUnits",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_OrganizationUnitRoles",
                table: "Base_OrganizationUnitRoles",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_NotificationSubscriptions",
                table: "Base_NotificationSubscriptions",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_Notifications",
                table: "Base_Notifications",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_LanguageTexts",
                table: "Base_LanguageTexts",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_Languages",
                table: "Base_Languages",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_Features",
                table: "Base_Features",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_EntityPropertyChanges",
                table: "Base_EntityPropertyChanges",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_EntityChangeSets",
                table: "Base_EntityChangeSets",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_EntityChanges",
                table: "Base_EntityChanges",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_Editions",
                table: "Base_Editions",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_BackgroundJobs",
                table: "Base_BackgroundJobs",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base_AuditLogs",
                table: "Base_AuditLogs",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Base_EntityChanges_Base_EntityChangeSets_EntityChangeSetId",
                table: "Base_EntityChanges",
                column: "EntityChangeSetId",
                principalTable: "Base_EntityChangeSets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_EntityPropertyChanges_Base_EntityChanges_EntityChangeId",
                table: "Base_EntityPropertyChanges",
                column: "EntityChangeId",
                principalTable: "Base_EntityChanges",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_Features_Base_Editions_EditionId",
                table: "Base_Features",
                column: "EditionId",
                principalTable: "Base_Editions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_OrganizationUnits_Base_OrganizationUnits_ParentId",
                table: "Base_OrganizationUnits",
                column: "ParentId",
                principalTable: "Base_OrganizationUnits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_Permissions_Base_Roles_RoleId",
                table: "Base_Permissions",
                column: "RoleId",
                principalTable: "Base_Roles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_Permissions_Base_Users_UserId",
                table: "Base_Permissions",
                column: "UserId",
                principalTable: "Base_Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_RoleClaims_Base_Roles_RoleId",
                table: "Base_RoleClaims",
                column: "RoleId",
                principalTable: "Base_Roles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_Roles_Base_Users_CreatorUserId",
                table: "Base_Roles",
                column: "CreatorUserId",
                principalTable: "Base_Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_Roles_Base_Users_DeleterUserId",
                table: "Base_Roles",
                column: "DeleterUserId",
                principalTable: "Base_Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_Roles_Base_Users_LastModifierUserId",
                table: "Base_Roles",
                column: "LastModifierUserId",
                principalTable: "Base_Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_Settings_Base_Users_UserId",
                table: "Base_Settings",
                column: "UserId",
                principalTable: "Base_Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_SubscriptionPayments_Base_Editions_EditionId",
                table: "Base_SubscriptionPayments",
                column: "EditionId",
                principalTable: "Base_Editions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_Tenants_Base_Users_CreatorUserId",
                table: "Base_Tenants",
                column: "CreatorUserId",
                principalTable: "Base_Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_Tenants_Base_Users_DeleterUserId",
                table: "Base_Tenants",
                column: "DeleterUserId",
                principalTable: "Base_Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_Tenants_Base_Editions_EditionId",
                table: "Base_Tenants",
                column: "EditionId",
                principalTable: "Base_Editions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_Tenants_Base_Users_LastModifierUserId",
                table: "Base_Tenants",
                column: "LastModifierUserId",
                principalTable: "Base_Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_UserClaims_Base_Users_UserId",
                table: "Base_UserClaims",
                column: "UserId",
                principalTable: "Base_Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_UserLogins_Base_Users_UserId",
                table: "Base_UserLogins",
                column: "UserId",
                principalTable: "Base_Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_UserRoles_Base_Users_UserId",
                table: "Base_UserRoles",
                column: "UserId",
                principalTable: "Base_Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_Users_Base_Users_CreatorUserId",
                table: "Base_Users",
                column: "CreatorUserId",
                principalTable: "Base_Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_Users_Base_Users_DeleterUserId",
                table: "Base_Users",
                column: "DeleterUserId",
                principalTable: "Base_Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_Users_Base_Users_LastModifierUserId",
                table: "Base_Users",
                column: "LastModifierUserId",
                principalTable: "Base_Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_UserTokens_Base_Users_UserId",
                table: "Base_UserTokens",
                column: "UserId",
                principalTable: "Base_Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Base_EntityChanges_Base_EntityChangeSets_EntityChangeSetId",
                table: "Base_EntityChanges");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_EntityPropertyChanges_Base_EntityChanges_EntityChangeId",
                table: "Base_EntityPropertyChanges");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_Features_Base_Editions_EditionId",
                table: "Base_Features");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_OrganizationUnits_Base_OrganizationUnits_ParentId",
                table: "Base_OrganizationUnits");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_Permissions_Base_Roles_RoleId",
                table: "Base_Permissions");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_Permissions_Base_Users_UserId",
                table: "Base_Permissions");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_RoleClaims_Base_Roles_RoleId",
                table: "Base_RoleClaims");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_Roles_Base_Users_CreatorUserId",
                table: "Base_Roles");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_Roles_Base_Users_DeleterUserId",
                table: "Base_Roles");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_Roles_Base_Users_LastModifierUserId",
                table: "Base_Roles");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_Settings_Base_Users_UserId",
                table: "Base_Settings");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_SubscriptionPayments_Base_Editions_EditionId",
                table: "Base_SubscriptionPayments");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_Tenants_Base_Users_CreatorUserId",
                table: "Base_Tenants");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_Tenants_Base_Users_DeleterUserId",
                table: "Base_Tenants");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_Tenants_Base_Editions_EditionId",
                table: "Base_Tenants");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_Tenants_Base_Users_LastModifierUserId",
                table: "Base_Tenants");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_UserClaims_Base_Users_UserId",
                table: "Base_UserClaims");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_UserLogins_Base_Users_UserId",
                table: "Base_UserLogins");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_UserRoles_Base_Users_UserId",
                table: "Base_UserRoles");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_Users_Base_Users_CreatorUserId",
                table: "Base_Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_Users_Base_Users_DeleterUserId",
                table: "Base_Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_Users_Base_Users_LastModifierUserId",
                table: "Base_Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_UserTokens_Base_Users_UserId",
                table: "Base_UserTokens");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_UserTokens",
                table: "Base_UserTokens");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_Users",
                table: "Base_Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_UserRoles",
                table: "Base_UserRoles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_UserOrganizationUnits",
                table: "Base_UserOrganizationUnits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_UserNotifications",
                table: "Base_UserNotifications");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_UserLogins",
                table: "Base_UserLogins");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_UserLoginAttempts",
                table: "Base_UserLoginAttempts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_UserClaims",
                table: "Base_UserClaims");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_UserAccounts",
                table: "Base_UserAccounts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_Tenants",
                table: "Base_Tenants");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_TenantNotifications",
                table: "Base_TenantNotifications");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_SubscriptionPayments",
                table: "Base_SubscriptionPayments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_Settings",
                table: "Base_Settings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_Roles",
                table: "Base_Roles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_RoleClaims",
                table: "Base_RoleClaims");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_PersistedGrants",
                table: "Base_PersistedGrants");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_Permissions",
                table: "Base_Permissions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_OrganizationUnits",
                table: "Base_OrganizationUnits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_OrganizationUnitRoles",
                table: "Base_OrganizationUnitRoles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_NotificationSubscriptions",
                table: "Base_NotificationSubscriptions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_Notifications",
                table: "Base_Notifications");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_LanguageTexts",
                table: "Base_LanguageTexts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_Languages",
                table: "Base_Languages");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_Features",
                table: "Base_Features");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_EntityPropertyChanges",
                table: "Base_EntityPropertyChanges");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_EntityChangeSets",
                table: "Base_EntityChangeSets");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_EntityChanges",
                table: "Base_EntityChanges");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_Editions",
                table: "Base_Editions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_BackgroundJobs",
                table: "Base_BackgroundJobs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base_AuditLogs",
                table: "Base_AuditLogs");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "Base.DictionaryValue");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "Base.DictionaryItem");

            migrationBuilder.RenameTable(
                name: "Base_UserTokens",
                newName: "Base.UserTokens");

            migrationBuilder.RenameTable(
                name: "Base_Users",
                newName: "Base.Users");

            migrationBuilder.RenameTable(
                name: "Base_UserRoles",
                newName: "Base.UserRoles");

            migrationBuilder.RenameTable(
                name: "Base_UserOrganizationUnits",
                newName: "Base.UserOrganizationUnits");

            migrationBuilder.RenameTable(
                name: "Base_UserNotifications",
                newName: "Base.UserNotifications");

            migrationBuilder.RenameTable(
                name: "Base_UserLogins",
                newName: "Base.UserLogins");

            migrationBuilder.RenameTable(
                name: "Base_UserLoginAttempts",
                newName: "Base.UserLoginAttempts");

            migrationBuilder.RenameTable(
                name: "Base_UserClaims",
                newName: "Base.UserClaims");

            migrationBuilder.RenameTable(
                name: "Base_UserAccounts",
                newName: "Base.UserAccounts");

            migrationBuilder.RenameTable(
                name: "Base_Tenants",
                newName: "Base.Tenants");

            migrationBuilder.RenameTable(
                name: "Base_TenantNotifications",
                newName: "Base.TenantNotifications");

            migrationBuilder.RenameTable(
                name: "Base_SubscriptionPayments",
                newName: "Base.SubscriptionPayments");

            migrationBuilder.RenameTable(
                name: "Base_Settings",
                newName: "Base.Settings");

            migrationBuilder.RenameTable(
                name: "Base_Roles",
                newName: "Base.Roles");

            migrationBuilder.RenameTable(
                name: "Base_RoleClaims",
                newName: "Base.RoleClaims");

            migrationBuilder.RenameTable(
                name: "Base_PersistedGrants",
                newName: "Base.PersistedGrants");

            migrationBuilder.RenameTable(
                name: "Base_Permissions",
                newName: "Base.Permissions");

            migrationBuilder.RenameTable(
                name: "Base_OrganizationUnits",
                newName: "Base.OrganizationUnits");

            migrationBuilder.RenameTable(
                name: "Base_OrganizationUnitRoles",
                newName: "Base.OrganizationUnitRoles");

            migrationBuilder.RenameTable(
                name: "Base_NotificationSubscriptions",
                newName: "Base.NotificationSubscriptions");

            migrationBuilder.RenameTable(
                name: "Base_Notifications",
                newName: "Base.Notifications");

            migrationBuilder.RenameTable(
                name: "Base_LanguageTexts",
                newName: "Base.LanguageTexts");

            migrationBuilder.RenameTable(
                name: "Base_Languages",
                newName: "Base.Languages");

            migrationBuilder.RenameTable(
                name: "Base_Features",
                newName: "Base.Features");

            migrationBuilder.RenameTable(
                name: "Base_EntityPropertyChanges",
                newName: "Base.EntityPropertyChanges");

            migrationBuilder.RenameTable(
                name: "Base_EntityChangeSets",
                newName: "Base.EntityChangeSets");

            migrationBuilder.RenameTable(
                name: "Base_EntityChanges",
                newName: "Base.EntityChanges");

            migrationBuilder.RenameTable(
                name: "Base_Editions",
                newName: "Base.Editions");

            migrationBuilder.RenameTable(
                name: "Base_BackgroundJobs",
                newName: "Base.BackgroundJobs");

            migrationBuilder.RenameTable(
                name: "Base_AuditLogs",
                newName: "Base.AuditLogs");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserTokens_TenantId_UserId",
                table: "Base.UserTokens",
                newName: "IX_Base.UserTokens_TenantId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserTokens_UserId",
                table: "Base.UserTokens",
                newName: "IX_Base.UserTokens_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Users_TenantId_NormalizedUserName",
                table: "Base.Users",
                newName: "IX_Base.Users_TenantId_NormalizedUserName");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Users_TenantId_NormalizedEmailAddress",
                table: "Base.Users",
                newName: "IX_Base.Users_TenantId_NormalizedEmailAddress");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Users_LastModifierUserId",
                table: "Base.Users",
                newName: "IX_Base.Users_LastModifierUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Users_DeleterUserId",
                table: "Base.Users",
                newName: "IX_Base.Users_DeleterUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Users_CreatorUserId",
                table: "Base.Users",
                newName: "IX_Base.Users_CreatorUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserRoles_TenantId_UserId",
                table: "Base.UserRoles",
                newName: "IX_Base.UserRoles_TenantId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserRoles_TenantId_RoleId",
                table: "Base.UserRoles",
                newName: "IX_Base.UserRoles_TenantId_RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserRoles_UserId",
                table: "Base.UserRoles",
                newName: "IX_Base.UserRoles_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserOrganizationUnits_TenantId_UserId",
                table: "Base.UserOrganizationUnits",
                newName: "IX_Base.UserOrganizationUnits_TenantId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserOrganizationUnits_TenantId_OrganizationUnitId",
                table: "Base.UserOrganizationUnits",
                newName: "IX_Base.UserOrganizationUnits_TenantId_OrganizationUnitId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserNotifications_UserId_State_CreationTime",
                table: "Base.UserNotifications",
                newName: "IX_Base.UserNotifications_UserId_State_CreationTime");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserLogins_TenantId_LoginProvider_ProviderKey",
                table: "Base.UserLogins",
                newName: "IX_Base.UserLogins_TenantId_LoginProvider_ProviderKey");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserLogins_TenantId_UserId",
                table: "Base.UserLogins",
                newName: "IX_Base.UserLogins_TenantId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserLogins_UserId",
                table: "Base.UserLogins",
                newName: "IX_Base.UserLogins_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserLoginAttempts_TenancyName_UserNameOrEmailAddress_Result",
                table: "Base.UserLoginAttempts",
                newName: "IX_Base.UserLoginAttempts_TenancyName_UserNameOrEmailAddress_Result");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserLoginAttempts_UserId_TenantId",
                table: "Base.UserLoginAttempts",
                newName: "IX_Base.UserLoginAttempts_UserId_TenantId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserClaims_TenantId_ClaimType",
                table: "Base.UserClaims",
                newName: "IX_Base.UserClaims_TenantId_ClaimType");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserClaims_UserId",
                table: "Base.UserClaims",
                newName: "IX_Base.UserClaims_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserAccounts_TenantId_UserName",
                table: "Base.UserAccounts",
                newName: "IX_Base.UserAccounts_TenantId_UserName");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserAccounts_TenantId_UserId",
                table: "Base.UserAccounts",
                newName: "IX_Base.UserAccounts_TenantId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserAccounts_TenantId_EmailAddress",
                table: "Base.UserAccounts",
                newName: "IX_Base.UserAccounts_TenantId_EmailAddress");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserAccounts_UserName",
                table: "Base.UserAccounts",
                newName: "IX_Base.UserAccounts_UserName");

            migrationBuilder.RenameIndex(
                name: "IX_Base_UserAccounts_EmailAddress",
                table: "Base.UserAccounts",
                newName: "IX_Base.UserAccounts_EmailAddress");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Tenants_TenancyName",
                table: "Base.Tenants",
                newName: "IX_Base.Tenants_TenancyName");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Tenants_SubscriptionEndDateUtc",
                table: "Base.Tenants",
                newName: "IX_Base.Tenants_SubscriptionEndDateUtc");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Tenants_LastModifierUserId",
                table: "Base.Tenants",
                newName: "IX_Base.Tenants_LastModifierUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Tenants_EditionId",
                table: "Base.Tenants",
                newName: "IX_Base.Tenants_EditionId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Tenants_DeleterUserId",
                table: "Base.Tenants",
                newName: "IX_Base.Tenants_DeleterUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Tenants_CreatorUserId",
                table: "Base.Tenants",
                newName: "IX_Base.Tenants_CreatorUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Tenants_CreationTime",
                table: "Base.Tenants",
                newName: "IX_Base.Tenants_CreationTime");

            migrationBuilder.RenameIndex(
                name: "IX_Base_TenantNotifications_TenantId",
                table: "Base.TenantNotifications",
                newName: "IX_Base.TenantNotifications_TenantId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_SubscriptionPayments_Status_CreationTime",
                table: "Base.SubscriptionPayments",
                newName: "IX_Base.SubscriptionPayments_Status_CreationTime");

            migrationBuilder.RenameIndex(
                name: "IX_Base_SubscriptionPayments_ExternalPaymentId_Gateway",
                table: "Base.SubscriptionPayments",
                newName: "IX_Base.SubscriptionPayments_ExternalPaymentId_Gateway");

            migrationBuilder.RenameIndex(
                name: "IX_Base_SubscriptionPayments_EditionId",
                table: "Base.SubscriptionPayments",
                newName: "IX_Base.SubscriptionPayments_EditionId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Settings_TenantId_Name",
                table: "Base.Settings",
                newName: "IX_Base.Settings_TenantId_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Settings_UserId",
                table: "Base.Settings",
                newName: "IX_Base.Settings_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Roles_TenantId_NormalizedName",
                table: "Base.Roles",
                newName: "IX_Base.Roles_TenantId_NormalizedName");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Roles_LastModifierUserId",
                table: "Base.Roles",
                newName: "IX_Base.Roles_LastModifierUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Roles_DeleterUserId",
                table: "Base.Roles",
                newName: "IX_Base.Roles_DeleterUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Roles_CreatorUserId",
                table: "Base.Roles",
                newName: "IX_Base.Roles_CreatorUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_RoleClaims_TenantId_ClaimType",
                table: "Base.RoleClaims",
                newName: "IX_Base.RoleClaims_TenantId_ClaimType");

            migrationBuilder.RenameIndex(
                name: "IX_Base_RoleClaims_RoleId",
                table: "Base.RoleClaims",
                newName: "IX_Base.RoleClaims_RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_PersistedGrants_SubjectId_ClientId_Type",
                table: "Base.PersistedGrants",
                newName: "IX_Base.PersistedGrants_SubjectId_ClientId_Type");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Permissions_UserId",
                table: "Base.Permissions",
                newName: "IX_Base.Permissions_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Permissions_RoleId",
                table: "Base.Permissions",
                newName: "IX_Base.Permissions_RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Permissions_TenantId_Name",
                table: "Base.Permissions",
                newName: "IX_Base.Permissions_TenantId_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Base_OrganizationUnits_TenantId_Code",
                table: "Base.OrganizationUnits",
                newName: "IX_Base.OrganizationUnits_TenantId_Code");

            migrationBuilder.RenameIndex(
                name: "IX_Base_OrganizationUnits_ParentId",
                table: "Base.OrganizationUnits",
                newName: "IX_Base.OrganizationUnits_ParentId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_OrganizationUnitRoles_TenantId_RoleId",
                table: "Base.OrganizationUnitRoles",
                newName: "IX_Base.OrganizationUnitRoles_TenantId_RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_OrganizationUnitRoles_TenantId_OrganizationUnitId",
                table: "Base.OrganizationUnitRoles",
                newName: "IX_Base.OrganizationUnitRoles_TenantId_OrganizationUnitId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_NotificationSubscriptions_TenantId_NotificationName_EntityTypeName_EntityId_UserId",
                table: "Base.NotificationSubscriptions",
                newName: "IX_Base.NotificationSubscriptions_TenantId_NotificationName_EntityTypeName_EntityId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_NotificationSubscriptions_NotificationName_EntityTypeName_EntityId_UserId",
                table: "Base.NotificationSubscriptions",
                newName: "IX_Base.NotificationSubscriptions_NotificationName_EntityTypeName_EntityId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_LanguageTexts_TenantId_Source_LanguageName_Key",
                table: "Base.LanguageTexts",
                newName: "IX_Base.LanguageTexts_TenantId_Source_LanguageName_Key");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Languages_TenantId_Name",
                table: "Base.Languages",
                newName: "IX_Base.Languages_TenantId_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Features_TenantId_Name",
                table: "Base.Features",
                newName: "IX_Base.Features_TenantId_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Base_Features_EditionId_Name",
                table: "Base.Features",
                newName: "IX_Base.Features_EditionId_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Base_EntityPropertyChanges_EntityChangeId",
                table: "Base.EntityPropertyChanges",
                newName: "IX_Base.EntityPropertyChanges_EntityChangeId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_EntityChangeSets_TenantId_UserId",
                table: "Base.EntityChangeSets",
                newName: "IX_Base.EntityChangeSets_TenantId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_EntityChangeSets_TenantId_Reason",
                table: "Base.EntityChangeSets",
                newName: "IX_Base.EntityChangeSets_TenantId_Reason");

            migrationBuilder.RenameIndex(
                name: "IX_Base_EntityChangeSets_TenantId_CreationTime",
                table: "Base.EntityChangeSets",
                newName: "IX_Base.EntityChangeSets_TenantId_CreationTime");

            migrationBuilder.RenameIndex(
                name: "IX_Base_EntityChanges_EntityTypeFullName_EntityId",
                table: "Base.EntityChanges",
                newName: "IX_Base.EntityChanges_EntityTypeFullName_EntityId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_EntityChanges_EntityChangeSetId",
                table: "Base.EntityChanges",
                newName: "IX_Base.EntityChanges_EntityChangeSetId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_BackgroundJobs_IsAbandoned_NextTryTime",
                table: "Base.BackgroundJobs",
                newName: "IX_Base.BackgroundJobs_IsAbandoned_NextTryTime");

            migrationBuilder.RenameIndex(
                name: "IX_Base_AuditLogs_TenantId_UserId",
                table: "Base.AuditLogs",
                newName: "IX_Base.AuditLogs_TenantId_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Base_AuditLogs_TenantId_ExecutionTime",
                table: "Base.AuditLogs",
                newName: "IX_Base.AuditLogs_TenantId_ExecutionTime");

            migrationBuilder.RenameIndex(
                name: "IX_Base_AuditLogs_TenantId_ExecutionDuration",
                table: "Base.AuditLogs",
                newName: "IX_Base.AuditLogs_TenantId_ExecutionDuration");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.UserTokens",
                table: "Base.UserTokens",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.Users",
                table: "Base.Users",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.UserRoles",
                table: "Base.UserRoles",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.UserOrganizationUnits",
                table: "Base.UserOrganizationUnits",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.UserNotifications",
                table: "Base.UserNotifications",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.UserLogins",
                table: "Base.UserLogins",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.UserLoginAttempts",
                table: "Base.UserLoginAttempts",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.UserClaims",
                table: "Base.UserClaims",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.UserAccounts",
                table: "Base.UserAccounts",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.Tenants",
                table: "Base.Tenants",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.TenantNotifications",
                table: "Base.TenantNotifications",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.SubscriptionPayments",
                table: "Base.SubscriptionPayments",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.Settings",
                table: "Base.Settings",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.Roles",
                table: "Base.Roles",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.RoleClaims",
                table: "Base.RoleClaims",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.PersistedGrants",
                table: "Base.PersistedGrants",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.Permissions",
                table: "Base.Permissions",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.OrganizationUnits",
                table: "Base.OrganizationUnits",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.OrganizationUnitRoles",
                table: "Base.OrganizationUnitRoles",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.NotificationSubscriptions",
                table: "Base.NotificationSubscriptions",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.Notifications",
                table: "Base.Notifications",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.LanguageTexts",
                table: "Base.LanguageTexts",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.Languages",
                table: "Base.Languages",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.Features",
                table: "Base.Features",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.EntityPropertyChanges",
                table: "Base.EntityPropertyChanges",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.EntityChangeSets",
                table: "Base.EntityChangeSets",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.EntityChanges",
                table: "Base.EntityChanges",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.Editions",
                table: "Base.Editions",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.BackgroundJobs",
                table: "Base.BackgroundJobs",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base.AuditLogs",
                table: "Base.AuditLogs",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Base.EntityChanges_Base.EntityChangeSets_EntityChangeSetId",
                table: "Base.EntityChanges",
                column: "EntityChangeSetId",
                principalTable: "Base.EntityChangeSets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.EntityPropertyChanges_Base.EntityChanges_EntityChangeId",
                table: "Base.EntityPropertyChanges",
                column: "EntityChangeId",
                principalTable: "Base.EntityChanges",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.Features_Base.Editions_EditionId",
                table: "Base.Features",
                column: "EditionId",
                principalTable: "Base.Editions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.OrganizationUnits_Base.OrganizationUnits_ParentId",
                table: "Base.OrganizationUnits",
                column: "ParentId",
                principalTable: "Base.OrganizationUnits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.Permissions_Base.Roles_RoleId",
                table: "Base.Permissions",
                column: "RoleId",
                principalTable: "Base.Roles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.Permissions_Base.Users_UserId",
                table: "Base.Permissions",
                column: "UserId",
                principalTable: "Base.Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.RoleClaims_Base.Roles_RoleId",
                table: "Base.RoleClaims",
                column: "RoleId",
                principalTable: "Base.Roles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.Roles_Base.Users_CreatorUserId",
                table: "Base.Roles",
                column: "CreatorUserId",
                principalTable: "Base.Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.Roles_Base.Users_DeleterUserId",
                table: "Base.Roles",
                column: "DeleterUserId",
                principalTable: "Base.Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.Roles_Base.Users_LastModifierUserId",
                table: "Base.Roles",
                column: "LastModifierUserId",
                principalTable: "Base.Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.Settings_Base.Users_UserId",
                table: "Base.Settings",
                column: "UserId",
                principalTable: "Base.Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.SubscriptionPayments_Base.Editions_EditionId",
                table: "Base.SubscriptionPayments",
                column: "EditionId",
                principalTable: "Base.Editions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.Tenants_Base.Users_CreatorUserId",
                table: "Base.Tenants",
                column: "CreatorUserId",
                principalTable: "Base.Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.Tenants_Base.Users_DeleterUserId",
                table: "Base.Tenants",
                column: "DeleterUserId",
                principalTable: "Base.Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.Tenants_Base.Editions_EditionId",
                table: "Base.Tenants",
                column: "EditionId",
                principalTable: "Base.Editions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.Tenants_Base.Users_LastModifierUserId",
                table: "Base.Tenants",
                column: "LastModifierUserId",
                principalTable: "Base.Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.UserClaims_Base.Users_UserId",
                table: "Base.UserClaims",
                column: "UserId",
                principalTable: "Base.Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.UserLogins_Base.Users_UserId",
                table: "Base.UserLogins",
                column: "UserId",
                principalTable: "Base.Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.UserRoles_Base.Users_UserId",
                table: "Base.UserRoles",
                column: "UserId",
                principalTable: "Base.Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.Users_Base.Users_CreatorUserId",
                table: "Base.Users",
                column: "CreatorUserId",
                principalTable: "Base.Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.Users_Base.Users_DeleterUserId",
                table: "Base.Users",
                column: "DeleterUserId",
                principalTable: "Base.Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.Users_Base.Users_LastModifierUserId",
                table: "Base.Users",
                column: "LastModifierUserId",
                principalTable: "Base.Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base.UserTokens_Base.Users_UserId",
                table: "Base.UserTokens",
                column: "UserId",
                principalTable: "Base.Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
