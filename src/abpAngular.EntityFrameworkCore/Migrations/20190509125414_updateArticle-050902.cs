﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace abpAngular.Migrations
{
    public partial class updateArticle050902 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Articles");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "CategoryId",
                table: "Articles",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
