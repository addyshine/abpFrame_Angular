﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using abpAngular.Configuration;
using abpAngular.Web;

namespace abpAngular.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class AbpFrameDbContextFactory : IDesignTimeDbContextFactory<AbpFrameDbContext>
    {
        public AbpFrameDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<AbpFrameDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder(), addUserSecrets: true);

            AbpFrameDbContextConfigurer.Configure(builder, configuration.GetConnectionString(AbpFrameConsts.ConnectionStringName));

            return new AbpFrameDbContext(builder.Options);
        }
    }
}