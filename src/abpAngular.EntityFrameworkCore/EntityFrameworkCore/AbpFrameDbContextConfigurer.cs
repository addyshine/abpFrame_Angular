using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace abpAngular.EntityFrameworkCore
{
    public static class AbpFrameDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<AbpFrameDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString, b=>b.UseRowNumberForPaging());
        }

        public static void Configure(DbContextOptionsBuilder<AbpFrameDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection, b => b.UseRowNumberForPaging());
        }
    }
}