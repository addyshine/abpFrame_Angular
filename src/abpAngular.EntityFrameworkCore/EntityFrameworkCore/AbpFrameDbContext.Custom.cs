﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Abp.IdentityServer4;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using abpAngular.Authorization.Roles;
using abpAngular.Authorization.Users;
using abpAngular.Chat;
using abpAngular.DataDictionary;
using abpAngular.Editions;
using abpAngular.Friendships;
using abpAngular.MultiTenancy;
using abpAngular.MultiTenancy.Accounting;
using abpAngular.MultiTenancy.Payments;
using abpAngular.Storage;
using abpAngular.EntityMapper.SubscriptionPayments;

namespace abpAngular.EntityFrameworkCore
{
    public partial class AbpFrameDbContext
    {
        /* Define an IDbSet for each entity of the application */



        public DbSet<abpAngular.Articles.ArticleCategory> ArticleCategorys { get; set; }
        
        public DbSet<abpAngular.Articles.Article> Articles { get; set; }
    }
}
