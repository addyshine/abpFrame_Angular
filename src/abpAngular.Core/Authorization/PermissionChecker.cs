﻿using Abp.Authorization;
using abpAngular.Authorization.Roles;
using abpAngular.Authorization.Users;

namespace abpAngular.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
