﻿using System.Threading.Tasks;
using Abp.Authorization.Users;
using abpAngular.Authorization.Users;

namespace abpAngular.Authorization
{
    public static class UserManagerExtensions
    {
        public static async Task<User> GetAdminAsync(this UserManager userManager)
        {
            return await userManager.FindByNameAsync(AbpUserBase.AdminUserName);
        }
    }
}
