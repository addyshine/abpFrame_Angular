﻿
using Abp;

namespace abpAngular
{
    public abstract class AbpFrameServiceBase : AbpServiceBase
    {
        /* Add your common members for all your domain services. */

        protected AbpFrameServiceBase()
        {
            LocalizationSourceName = AbpFrameConsts.LocalizationSourceName;
        }
    }
}
