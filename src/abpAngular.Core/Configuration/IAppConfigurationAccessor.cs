﻿using Microsoft.Extensions.Configuration;

namespace abpAngular.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}
