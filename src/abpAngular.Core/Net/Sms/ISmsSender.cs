using System.Threading.Tasks;

namespace abpAngular.Identity
{
    public interface ISmsSender
    {
        Task SendAsync(string number, string message);
    }
}