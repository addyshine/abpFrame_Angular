﻿using Abp.Domain.Services;

namespace abpAngular
{
    public abstract class AbpFrameDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected AbpFrameDomainServiceBase()
        {
            LocalizationSourceName = AbpFrameConsts.LocalizationSourceName;
        }
    }
}
