﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace abpAngular.Articles
{
    /// <summary>
    /// 文章类别
    /// </summary>
    [Description("文章类别")]
    [Display(Name = "文章类别")]
    public class ArticleCategory : FullAuditedEntity<long>, IMayHaveTenant
    {
        /// <summary>
        /// 类别名称
        /// </summary>
        [Display(Name = "类别名称")]
        [Required]
        [MaxLength(50, ErrorMessage = "类别名称超出最大长度")]
        public string CategoryName { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Display(Name = "备注")]
        [MaxLength(100, ErrorMessage = "备注超出最大长度")]
        public string Remark { get; set; }

        public virtual ICollection<Article> Articles { get; set; }

        /// <summary>
        /// 所属租户
        /// </summary>
        public int? TenantId { get; set; }
    }
}
