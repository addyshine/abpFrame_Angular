﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace abpAngular.Articles
{
    /// <summary>
    /// 文章模块
    /// </summary>
    [Description("文章模块")]
    [Display(Name = "文章模块")]
    public class Article : FullAuditedEntity<long>, IMayHaveTenant
    {
        /// <summary>
        /// 文章标题
        /// </summary>
        [Display(Name = "文章标题")]
        [Required]
        [MaxLength(50, ErrorMessage = "文章标题超出最大长度")]
        public string Title { get; set; }

        public virtual ArticleCategory ArticleCategory { get; set; }

        /// <summary>
        /// 发布时间
        /// </summary>
        [Display(Name = "发布时间")]
        public DateTime? PublishTime { get; set; }

        /// <summary>
        /// 文章内容
        /// </summary>
        [Display(Name = "文章内容")]
        public string Content { get; set; }

        /// <summary>
        /// 所属租户
        /// </summary>
        public int? TenantId { get; set; }
    }
}
