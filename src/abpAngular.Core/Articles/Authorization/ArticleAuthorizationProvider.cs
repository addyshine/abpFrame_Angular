﻿using Abp.Authorization;

namespace abpAngular.Authorization
{
    public partial class AppAuthorizationProvider
    {
        public void SetArticlePermissions(Permission root)
        {
            // 在这里配置了文章模块 的权限。
            var entityPermission = root.CreateChildPermission(ArticlePermissions.Node, L("文章模块"));
            entityPermission.CreateChildPermission(ArticlePermissions.Create, L("新增"));
            entityPermission.CreateChildPermission(ArticlePermissions.Edit, L("编辑"));
            entityPermission.CreateChildPermission(ArticlePermissions.Delete, L("删除"));
            entityPermission.CreateChildPermission(ArticlePermissions.BatchDelete, L("批量删除"));
            entityPermission.CreateChildPermission(ArticlePermissions.Export, L("导出"));
        }
    }
}
