﻿namespace abpAngular.Authorization
{
    /// <summary>
    /// 定义权限常量
    /// </summary>
    public static class ArticleCategoryPermissions
    {
        /// <summary>
        /// 模块权限
        /// </summary>
        public const string Node = "Pages.ArticleCategory";

        /// <summary>
        /// 新增权限
        /// </summary>
        public const string Create = "Pages.ArticleCategory.Create";

        /// <summary>
        /// 编辑权限
        /// </summary>
        public const string Edit = "Pages.ArticleCategory.Edit";

        /// <summary>
        /// 删除权限
        /// </summary>
        public const string Delete = "Pages.ArticleCategory.Delete";

        /// <summary>
        /// 批量删除权限
        /// </summary>
        public const string BatchDelete = "Pages.ArticleCategory.BatchDelete";

        /// <summary>
        /// 导出权限
        /// </summary>
        public const string Export = "Pages.ArticleCategory.Export";
    }
}
