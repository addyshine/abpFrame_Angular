﻿using Abp.Authorization;

namespace abpAngular.Authorization
{
    public partial class AppAuthorizationProvider
    {
        public void SetArticleCategoryPermissions(Permission root)
        {
            // 在这里配置了文章类别 的权限。
            var entityPermission = root.CreateChildPermission(ArticleCategoryPermissions.Node, L("文章类别"));
            entityPermission.CreateChildPermission(ArticleCategoryPermissions.Create, L("新增"));
            entityPermission.CreateChildPermission(ArticleCategoryPermissions.Edit, L("编辑"));
            entityPermission.CreateChildPermission(ArticleCategoryPermissions.Delete, L("删除"));
            entityPermission.CreateChildPermission(ArticleCategoryPermissions.BatchDelete, L("批量删除"));
            entityPermission.CreateChildPermission(ArticleCategoryPermissions.Export, L("导出"));
        }
    }
}
