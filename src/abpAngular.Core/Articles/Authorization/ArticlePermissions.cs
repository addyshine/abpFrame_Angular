﻿namespace abpAngular.Authorization
{
    /// <summary>
    /// 定义权限常量
    /// </summary>
    public static class ArticlePermissions
    {
        /// <summary>
        /// 模块权限
        /// </summary>
        public const string Node = "Pages.Article";

        /// <summary>
        /// 新增权限
        /// </summary>
        public const string Create = "Pages.Article.Create";

        /// <summary>
        /// 编辑权限
        /// </summary>
        public const string Edit = "Pages.Article.Edit";

        /// <summary>
        /// 删除权限
        /// </summary>
        public const string Delete = "Pages.Article.Delete";

        /// <summary>
        /// 批量删除权限
        /// </summary>
        public const string BatchDelete = "Pages.Article.BatchDelete";

        /// <summary>
        /// 导出权限
        /// </summary>
        public const string Export = "Pages.Article.Export";
    }
}
