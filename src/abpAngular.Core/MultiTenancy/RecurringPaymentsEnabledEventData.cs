﻿using Abp.Events.Bus;

namespace abpAngular.MultiTenancy
{
    public class RecurringPaymentsEnabledEventData : EventData
    {
        public int TenantId { get; set; }
    }
}