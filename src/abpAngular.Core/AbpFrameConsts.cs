﻿namespace abpAngular
{
    public class AbpFrameConsts
    {
        public const string LocalizationSourceName = "AbpFrame";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;

        public const string Currency = "CNY";

        public const string CurrencySign = "￥";


        public static class DbTablePrefix
        {
            public const string Base = "Base_";

            public const string APP = "APP_";
        }
    }
}
