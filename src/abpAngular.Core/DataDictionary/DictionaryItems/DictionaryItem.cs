﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace abpAngular.DataDictionary
{
    [Table("Base.DictionaryItem")]
    public class DictionaryItem: FullAuditedEntity<int>, IMayHaveTenant
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MaxLength(50)]
        public string Code { get; set; }


        public int? ParentId { get; set; }
        
        public int SortCode { get; set; }

        public virtual ICollection<DictionaryValue> DictionaryValues { get; set; }
        public int? TenantId { get; set; }
    }
}
