﻿namespace abpAngular.Authorization
{
    /// <summary>
    /// 定义权限常量
    /// </summary>
    public static class DictionaryItemPermissions
    {
        /// <summary>
        /// 模块权限
        /// </summary>
        public const string Node = "Pages.DictionaryItem";

        /// <summary>
        /// 新增权限
        /// </summary>
        public const string Create = "Pages.DictionaryItem.Create";

        /// <summary>
        /// 编辑权限
        /// </summary>
        public const string Edit = "Pages.DictionaryItem.Edit";

        /// <summary>
        /// 删除权限
        /// </summary>
        public const string Delete = "Pages.DictionaryItem.Delete";

        /// <summary>
        /// 批量删除权限
        /// </summary>
        public const string BatchDelete = "Pages.DictionaryItem.BatchDelete";

        /// <summary>
        /// 导出权限
        /// </summary>
        public const string Export = "Pages.DictionaryItem.Export";

    }
}
