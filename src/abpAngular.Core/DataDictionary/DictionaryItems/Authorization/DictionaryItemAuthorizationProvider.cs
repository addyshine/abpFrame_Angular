﻿using Abp.Authorization;

namespace abpAngular.Authorization
{
    public partial class AppAuthorizationProvider
    {
        public void SetDictionaryItemPermissions(Permission root)
        {
            // 在这里配置了DictionaryItem 的权限。
            var entityPermission = root.CreateChildPermission(DictionaryItemPermissions.Node, L("DictionaryItem"));
            entityPermission.CreateChildPermission(DictionaryItemPermissions.Create, L("CreateDictionaryItem"));
            entityPermission.CreateChildPermission(DictionaryItemPermissions.Edit, L("EditDictionaryItem"));
            entityPermission.CreateChildPermission(DictionaryItemPermissions.Delete, L("DeleteDictionaryItem"));
            entityPermission.CreateChildPermission(DictionaryItemPermissions.BatchDelete, L("BatchDeleteDictionaryItem"));
            entityPermission.CreateChildPermission(DictionaryItemPermissions.Export, L("ExportDictionaryItem"));
        }
    }
}
