﻿using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using abpAngular.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace abpAngular.Authorization
{
    public partial class AppAuthorizationProvider
    {

        public void SetDictionaryValuePermissions(Permission root)
        {
            // 在这里配置了DictionaryValue 的权限。
            var entityPermission = root.CreateChildPermission(DictionaryValuePermissions.Node, L("DictionaryValue"));
            entityPermission.CreateChildPermission(DictionaryValuePermissions.Create, L("CreateDictionaryValue"));
            entityPermission.CreateChildPermission(DictionaryValuePermissions.Edit, L("EditDictionaryValue"));
            entityPermission.CreateChildPermission(DictionaryValuePermissions.Delete, L("DeleteDictionaryValue"));
            entityPermission.CreateChildPermission(DictionaryValuePermissions.BatchDelete, L("BatchDeleteDictionaryValue"));
            entityPermission.CreateChildPermission(DictionaryValuePermissions.Export, L("ExportDictionaryValue"));
        }
    }
}
