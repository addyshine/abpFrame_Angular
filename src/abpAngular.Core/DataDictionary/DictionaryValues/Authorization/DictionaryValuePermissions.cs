﻿using System;
using System.Collections.Generic;
using System.Text;

namespace abpAngular.Authorization
{
    /// <summary>
    /// 定义权限常量
    /// </summary>
    public static class DictionaryValuePermissions
    {
        /// <summary>
        /// 模块权限
        /// </summary>
        public const string Node = "Pages.DictionaryValue";

        /// <summary>
        /// 新增权限
        /// </summary>
        public const string Create = "Pages.DictionaryValue.Create";

        /// <summary>
        /// 编辑权限
        /// </summary>
        public const string Edit = "Pages.DictionaryValue.Edit";

        /// <summary>
        /// 删除权限
        /// </summary>
        public const string Delete = "Pages.DictionaryValue.Delete";

        /// <summary>
        /// 批量删除权限
        /// </summary>
        public const string BatchDelete = "Pages.DictionaryValue.BatchDelete";

        /// <summary>
        /// 导出权限
        /// </summary>
        public const string Export = "Pages.DictionaryValue.Export";

    }
}
